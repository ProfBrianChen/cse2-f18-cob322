/*
Group #: L
Group Members: Kirk, Alana, Cody
Program Description: A basic Venmo menu system that uses methods instead of copy and pasted code
**/

import java.util.Scanner;

public class DoWhileMethods{
  public static void displayMenu()
  {
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Quit");
  }
  public static double sendMoney(double balance, Scanner input)
  {
    String friend;
       double money = 0; //money sent is local to this method, along with string.
                          //main doesn't need to be concerned with these values as it only returns balance, the only thing that is cross-method
     //display balance
                System.out.println("You have $" + balance + " in your account" );
                System.out.println("How much money do you want to send?"); 
                money = checkForBadInputDouble(input);
                input.nextLine(); //clear buffer
                
                //check valid amount no negative and if enough money is in balance to send money 
                if(money <= 0)
                  System.out.println("Invalid amount entered");
                else if(money <= balance){
                 // balance -= money;
                  System.out.println("Who do you want to send your money to?");
                  friend = input.nextLine();
                  System.out.println("You have successfully sent "+ money +" to "+ friend);
                  
                }
                else{
                  System.out.println("You do not have sufficient funds for this transaction.");
                }
                return money;
  }
  public static double requestMoney(double balance, Scanner input)
  {
        String friend;
        double money = 0; //money and friend are local to this method as they are never reused
                System.out.println("Who do you want to request money from?");
                friend = input.nextLine();
                System.out.println("How much money do you want to request?");
                
                // check user input for double value
                money = checkForBadInputDouble(input);
                input.nextLine();
                
                // friend info
                System.out.println("You are requesting $" + money + " from "+ friend);
                return money;
                
                //add money to balance and confirm
  }
  public static void exitProgram()
  {
    System.out.println("Goodbye.");
    System.exit(0); //call exit and pass a non-error exit code
  }
  public static int checkForBadInputInt(Scanner input)
  {
    int choice = 0;
    while(input.hasNextInt() == false){
        System.out.println("You entered and invalid value -- try again");
        input.nextLine();//clear buffer
      }
      choice = input.nextInt();
      input.nextLine(); //clear buffer
      return choice;
  } 
    public static double checkForBadInputDouble(Scanner input)
  {
    double choice = 0;
    while(input.hasNextDouble() == false){
        System.out.println("You entered and invalid value -- try again");
        input.nextLine();//clear buffer
      }
      choice = input.nextDouble();
      input.nextLine(); //clear buffer
      return choice;
  } 
  public static void handleDefault()
  {
    System.out.println("you entered an invalid value -- try again");
    //do something else if a non-default input is given
  }
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    int choice;
    double balance = 100; //keep balance declared in main method so we can keep it from being local to every function 
    do{
      //display menu
      displayMenu();
      //get and check user input for an integer
      choice = checkForBadInputInt(input);
      
      switch (choice){
             //algorithm for send money
        case 1: balance -= sendMoney(balance, input); //balance is kept in the main method scope, so we return how much we sent and subtract it
                 break;
                 //algorithm for request money
        case 2: balance += requestMoney(balance, input); //balance is kept in the main method scope, so we return how much we request and add it
                System.out.println("Once confirmed, your new balance will be $" + balance);
                break;
                //get balance info    
        case 3: System.out.println("You have $" + balance + " in your account" ); //no method for this as balance is in the scope of this method
                break;
                //user wants to quit
        case 4: exitProgram(); //quit the program if user enters 4
                break;
                 //invalid value entered
        default: handleDefault(); //handles the default value by printing that the user did an invalid input. Made a method so that additionally functionality could be added
                break;
      }
    }while(choice != 4);
  }
}
    