/*
Group #: L
Group Members: Kirk, Alana, Cody
Program Description: This program should imitate a basic Venmo menu system. Until the user types in a "4", the menu will continue to loop through and process transactions. Each choice (1-4) prompts the computer to complete a different function. For example, if the user types 3, the user's current balance should be printed out.

Before starting to add code, you should walk through the CT planning process. We have included multiple questions to guide the process of decomposition/algorithmic thinking.
*/

import java.util.Scanner;
public class DoWhile{
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    int choice = 0; //choice will hold the users selection/input
    double userBalance = 200.75; //balance of the user
    double amountSent = 0;
    String sender;
    String recipient;
    double amountReq = 0; 
    String junk = "";
    // input-- what variables will be needed?
    
    
    
    
    
    //So far, the do-while loop simply prints out the menu options. What else needs to happen here?
    do{
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Quit");
      System.out.println("Please enter a choice");
      //If the user types 1, what should the program do? Use psuedocode or english words in comments
      while(!input.hasNextInt())
      {
        System.out.println("Invalid input, try again.");
        junk = input.next();
      }

      choice = input.nextInt();
      if(choice == 1)
      {
        System.out.println("Enter the recipient of the $: ");
        recipient = input.next();
        System.out.println("Enter the amount to send: ");
        while(!input.hasNextDouble())
        {
          System.out.println("Invalid input, try again.");
          junk = input.next();
        }
        amountSent = input.nextDouble();
        userBalance -= amountSent;
        System.out.println("You sent "+amountSent+" to "+recipient);

      }

      
      //If the user types 1, what is the expected output? Use psuedocode or english words in comments
      
      
      
      
      //If the user types 2, what should the program do? Use psuedocode or english words in comments
      if(choice == 2)
      {
        System.out.println("Enter sender");
        sender = input.nextLine();
        System.out.println("Enter the amount to request");
        while(!input.hasNextDouble())
        {
          System.out.println("Invalid input, try again.");
          junk = input.next();
        }
        amountReq = input.nextDouble();
        userBalance += amountReq;
        System.out.println("");
        System.out.println("You requested "+amountReq+" from "+sender);
      }
      
      
      //If the user types 2, what is the expected output? Use psuedocode or english words in comments
      
      
      
      
      
      //If the user types 3, what should the program do? Use psuedocode or english words in comments
      if(choice == 3)
      {
        System.out.println("Your balance is: "+userBalance);
      }
      
      
      
      //If the user types 3, what is the expected output? Use psuedocode or english words in comments
      
      
      
      
      
      if(choice == 4)
      {
        System.out.println("Goodbye.");
      }
      //if the user types 4, what should the program do? Use psuedocode or english words in comments
 
      
      
      
      
      //If the user types 4, what is the expected output? Use psuedocode or english words in comments
      

      
      
      
      //Implement the program functionality that you defined below:
      
      
      
      

    }while(choice != 4);
  }
}
    