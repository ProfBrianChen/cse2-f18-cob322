import java.util.Scanner;
public class DoWhileExample{
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    int choice;
    double balance = 100;
    double money = 0;
    String friend;
    
    do{
      //display menu
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Quit");
      
      //get and check user input for an integer
      while(input.hasNextInt() == false){
        System.out.println("You entered and invalid value -- try again");
        input.nextLine();//clear buffer
      }
      choice = input.nextInt();
      input.nextLine(); //clear buffer
      
      switch (choice){
        
             //algorithm for send money
        case 1: 
                //display balance
                System.out.println("You have $" + balance + " in your account" );
                System.out.println("How much money do you want to send?");
                
                 //get and check user input for a double
                while(input.hasNextDouble() == false){
                 System.out.println("You entered and invalid value -- try again");
                 input.nextLine(); //clear buffer
                } 
                money = input.nextDouble();
                input.nextLine(); //clear buffer
                
                //check valid amount no negative and if enough money is in balance to send money 
                if(money <= 0)
                  System.out.println("Invalid amount entered");
                else if(money <= balance){
                  balance -= money;
                  System.out.println("Who do you want to send your money to?");
                  friend = input.nextLine();
                  System.out.println("You have successfully sent "+ money +" to "+ friend);
                }
                else{
                  System.out.println("You do not have sufficient funds for this transaction.");
                }
                 break;
                 
                 //algorithm for request money
        case 2: System.out.println("Who do you want to request money from?");
                friend = input.nextLine();
                System.out.println("How much money do you want to request?");
                
                // check user input for double value
                while(input.hasNextDouble() == false){
                 System.out.println("You entered and invalid value -- try again");
                 input.nextLine();
                } 
                money = input.nextDouble();
                input.nextLine();
                
                // friend info
                System.out.println("You are requesting $" + money + " from "+ friend);
                
                //add money to balance and confirm
                balance += money;
                System.out.println("Once confirmed, your new balance will be $" + balance);
                break;
                
                //get balance info    
        case 3: System.out.println("You have $" + balance + " in your account" );
                break;
                
                //user wants to quit
        case 4: System.out.println("Goodbye");
                break;
                
                 //invalid value entered
        default: System.out.println("you entered an invalid value -- try again");
                break;
      }
    }while(choice != 4);
  }
}
    