/**
Cody Benkoski, Kirkland Cobb, Alana Hill
CSE 002
Venmo Activity 7
**/
import java.util.Scanner;
public class VenmoMenuMethods{
  public static void main(String [] args){
  	String [] friendNames = new String[5];
  	friendNames[0] = "Kallie";
  	friendNames[1] = "Jim";
  	friendNames[2] = "Dave";
  	int index = 2;
  	double [] moneySent = new double[5];
  	moneySent[0] = 23.4;
  	moneySent[1] = 17.8;
  	moneySent[2] = 9.87;
  	double [] moneyReceived = new double[5];
  	moneyReceived[0] = 54.34;
  	moneyReceived[1] = 34.98;
  	moneyReceived[2] = 23.54;
    Scanner input = new Scanner(System.in);
    int choice;
    double balance = 100;
    double money = 0;
    String friend;
    String newfriend;
    String junk;
    do{
      printMenu();
      choice = getInt();   
      switch (choice){
        case 1: getBalance(balance);
                balance = sendMoney(balance, friendNames, moneyReceived);    
                break;
        case 2: balance = requestMoney(balance);
                break;
        case 3: getBalance(balance);
                break;
        case 4: addFriend(friendNames,index);
                break;
        case 5: printFriendsReport(friendNames, moneySent, moneyReceived, index);
                break;
        case 6: System.out.println("Goodbye");
                break;
        default: System.out.println("you entered an invalid value -- try again");
                break;
      }
    }while(choice != 4);
  }
  
  
  //print Venmo Main Menu
  public static void printMenu(){
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Add friend");
      System.out.println("5. Print Friend Report");
      System.out.println("6. Quit");
  }
  
  //get a number and check for an integer
  public static int getInt(){
    Scanner input = new Scanner(System.in);
     while(input.hasNextInt() == false){
        System.out.println("You entered and invalid value -- try again");
        input.nextLine();//clear buffer
     }
     return input.nextInt();
  } 
  public static void getBalance(double balance){
     System.out.println("You have $" + balance + " in your account" );
  }
  
  //get a number and check for a double
  public static double getDouble(){
    Scanner input = new Scanner(System.in);
    while(input.hasNextDouble() == false){
         System.out.println("You entered and invalid value -- try again");
         input.nextLine(); //clear buffer
     } 
     return input.nextDouble();
  }
  
  //driver method for sendMondy
  public static double sendMoney(double balance, String[] friends, double[] moneyReceived){
      double money = 0;
      System.out.println("How much money do you want to send?");
      money = getDouble();//get and check user input for a double
      balance = checkMoney(money,balance, friends, moneyReceived); //check valid amount no negative and if enough money is in balance to send money 
      return balance;
  }
  
  //method prints friend and money sent to friend
  public static void getFriend(double money, String[] friends, double[] moneyReceived){
        Scanner input = new Scanner(System.in);
        System.out.println("Who do you want to send your money to?");
        String friend = input.nextLine();
        int index =0;
        for (index = 0;index < friends.length; index++ ) 
        {
        	if(friends[index].equals(friend))
        	{
            moneyReceived[index] += money;
        		System.out.println("You sent $"+money+" to "+friend);
        		break;
        	}	
        }
        System.out.println("Friend not found :(((");
  }
  
  /*method that makes sure money entered is greater than 0, 
   * there are sufficient funds in balance, if sufficient funds the balance is changed
   * and returned. if funds are not sufficient the user is taken back to the main menu
   */
  public static double checkMoney(double money, double balance, String[] friends, double[] moneyReceived){
        while (money <= 0){
            System.out.println("Invalid amount entered");
            money = getDouble();
         }
         if (money <= balance){
             balance -= money;
             getFriend(money, friends, moneyReceived);
           }
          else{
             System.out.println("You do not have sufficient funds for this transaction.");
          }
          return balance;
  }
  
  //driver method for requestMonday
  public static double requestMoney(double balance){
     System.out.println("How much money do you want to request?");
     double money = getDouble(); 
     balance = checkRequestMoney(money,balance);
     System.out.println("Once confirmed, your new balance will be $" + balance);
     return balance;
               
}
  
  //gets friends information for the Request menu item and prints the amount requested from friend
  public static void getFriendRequest(double money){
     Scanner input = new Scanner(System.in);
     System.out.println("Who do you want to request money from?");
     String friend = input.nextLine();  
     System.out.println("You are requesting $" + money + " from "+ friend);
  }
  
  //method makes sure money is greater than 0 and adds requested amount to balance and returns new balance
  public static double checkRequestMoney(double money, double balance){
      while (money <= 0){
           System.out.println("Invalid amount entered");
           money = getDouble();
      }
      balance += money;
      getFriendRequest(money);
      return balance;
  }

  public static void addFriend(String[] friendNames, int index)
  {
  	Scanner input = new Scanner(System.in);
    System.out.println("What's your new friend's name?");
    String newfriend = input.nextLine();  
  	if(friendNames.length < index+1)
  	{
  		System.out.println("No more friends allowed");
  	}
  	else
  	{

  		friendNames[index+1] = newfriend;
  		System.out.println("Added "+newfriend+" to friends");
  	}
  	
  }

  public static void printFriendsReport(String [] friendNames, double[] moneySent, double[] moneyReceived, int length)
  {
  	System.out.println("Friends Name:		Money Sent:			Money Received:");
  	for (int index =0;index <= length;index++ ) 
  	{
  		System.out.println(friendNames[index]+"			$"+moneySent[index]+"					$"+moneyReceived[index])	;
  	}
  }
  
}              
    
                
                
             