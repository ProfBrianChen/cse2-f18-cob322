/**
Group L
Kirk, Alana, Cody
Venmo Activity
10-1-2018
**/
import java.util.Scanner;

public class ReportWhile
{
  public static void main(String args[])
  {
    double maxValue = 0.0;
    double userValue;
    double totalValue = 0.0;
    double averageVal = 0.0;
    int count = 0;
    
    Scanner scnr = new Scanner(System.in);
        System.out.println("Enter your transaction, or enter a negative number to stop: ");
      userValue = scnr.nextDouble();
       while(userValue > 0)
    {
    System.out.println("Enter your transaction, or enter a negative number to stop: ");
      userValue = scnr.nextDouble();
      if(userValue < 0)
      {
        break;
      }
      totalValue += userValue;
      if(userValue > maxValue)
      {
        maxValue = userValue;
      }
      
      count++;
    }
      averageVal = (totalValue/(double)count);
    System.out.println("Transaction max: "+maxValue);
    System.out.println("Transaction Averages: "+averageVal);
    System.out.println("Total value: "+totalValue);
    
  }
}