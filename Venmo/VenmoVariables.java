/**
Group L: Kirk, Cody, Alana
Class Activity 2
September 3
Program Description: Declaring, initializing and displaying variables for the venmo system
**/
public class VenmoVariables
{
public static void main(String args[])
{
  //begin declared variables
  int dollarAmt;
  double bankBalanceSndr;
  double venmoBalanceSndr;
  double bankBalanceRcpnt;
  double venmoBalanceRcpnt;
  double venmoBalanceSndrNew;
  double venmoBalanceRcpntNew;
  int phoneNum;
  //end declared variables
  
  //begin initialized variables
  dollarAmt = 10;
  bankBalanceSndr = 200;
  venmoBalanceSndr = 20.75;
  bankBalanceRcpnt = 4568.42;
  venmoBalanceRcpnt = 0.0;
  venmoBalanceRcpntNew = 0.0;
  venmoBalanceSndrNew = 0.0;
  phoneNum = 1234567890;
  //end initialized variables
  
  //begin computations
  venmoBalanceSndrNew = venmoBalanceSndr - dollarAmt; //subtract amount sent from sender, and update the variable
  venmoBalanceRcpntNew = venmoBalanceRcpnt + dollarAmt; //add amount sent, and update the variable
  //end computations
  
  //begin print statements
  System.out.println("Amount to be transferred: $"+ dollarAmt);
  System.out.println("Phone number of individual receiving $: "+ phoneNum);
  System.out.println("Sender's Venmo Balance: $" +venmoBalanceSndr);
  System.out.println("Recipient's Venmo Balance: $"+venmoBalanceRcpnt);
  System.out.println("Sender's new balance: $" +venmoBalanceSndrNew);
  System.out.println("Recipient's new balance: $"+venmoBalanceRcpntNew);
  
}
}