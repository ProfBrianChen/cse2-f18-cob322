/** 
Cody Benkoski
10-5-2018
CSE 002
**/

import java.util.Scanner;

public class TwistGenerator
{
	public static void main(String args[]) //takes user input (in the form of an integer) and uses it to generate a twist of that length
	{
		int length = 0; //length of the twist
		int i; //counter
		String junk; //used to store the unwanted remaining buffer

		Scanner scnr = new Scanner(System.in); //create a new scanner object 

		System.out.println("Please enter a valid integer to be used for the twist length: "); //prompt the user for the length of the twist

		while(!scnr.hasNextInt())
		{
			System.out.print("You have entered an invalid input, please try again:");

			junk = scnr.nextLine(); //clear the buffer
		}

		while(scnr.hasNextInt())
		{
			length = scnr.nextInt();
			if(length < 0) //check for negative 
			{
				System.out.print("You have entered an invalid input, please try again:");
			}
			else
			{
				break;
			}
		}

		if(length == 0)
		{
			System.out.println("You have entered zero, while acceptable, nothing will be printed.");
		}
		i = 0;
		int z = 0;
		//begin "perfect" segments
		if((length % 3) == 0)
		{
			z = length / 3;
			while(i < z)
			{
				System.out.print("\\ /");
				i++;
			}
			System.out.print("\n");
			i = 0;
			while(i < z)
			{
				System.out.print(" X ");
				i++;
			}
			System.out.print("\n");
			i = 0;
			while(i < z)
			{
				System.out.print("/ \\");
				i++;
			}
		}
		else //tests if it is caught in a 1/3 length, if not, it'll round up //if( (length / 3) == (int)(length / 3.0))
		{
			z = length / 3;
			
			while(i < z)
			{
				System.out.print("\\ /");
				i++;
			}
			System.out.print("\\");
			System.out.print("\n");
			i = 0;
			
			while(i < z)
			{
				System.out.print(" X ");
				i++;
			}
			System.out.print(" ");
			System.out.print("\n");
			i = 0;
			
			while(i < z)
			{
				System.out.print("/ \\");
				i++;
			}
			System.out.print("/ ");

			
			
		}
		//end "perfect" case
/**
\ /\ /\ /\ /\ /\ /\ /\ /\
 X  X  X  X  X  X  X  X  
/ \/ \/ \/ \/ \/ \/ \/ \/


**/



	}
}