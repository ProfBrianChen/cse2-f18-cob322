/**
Cody Benkoski
Lab 06
10-10-2018
**/

import java.util.Scanner;

public class PatternD
{
	public static void main(String args[])
	{
		int pyramidLength = 0; //length of pyramid entered by User
		Scanner input = new Scanner(System.in); //scanner class
		String junk;

		System.out.println("Enter an integer 1-10 for the length of the pyramid"+"\n"); //prompt the user

		while(!input.hasNextInt())
		{
			System.out.println("Invalid input, please try again"); //check for bad input
			junk = input.nextLine();
		}

		
		pyramidLength = input.nextInt();
		System.out.println(""); //used to seperate the input from the pyramid
			if(pyramidLength > 10 || pyramidLength < 0)
			{
				System.out.println("Invalid input, goodbye."); //check for bad input
				return;
			}
			else
			{
				 for(int numrows = pyramidLength; numrows > 0; numrows--) 
				 {
				 	for(int i = numrows; i > 0; i--) //loop through number of remaining rows, subtracting one to get "flip" the numbers
				 	{
				 		System.out.print(i+" "); //print the number with a space to its right
				 	}
				 	System.out.println(""); //add a line break
				 }
			}


	}
}