/**
Cody Benkoski
Lab 06
10-10-2018
**/

import java.util.Scanner;

public class PatternC
{
	public static void main(String args[])
	{
		int pyramidLength = 0; //length of pyramid entered by User
		Scanner input = new Scanner(System.in); //scanner class
		String junk;

		System.out.println("Enter an integer 1-10 for the length of the pyramid"); //prompt the user

		while(!input.hasNextInt())
		{
			System.out.println("Invalid input, please try again"); //check for invalid items
			junk = input.nextLine();
		}

		
		pyramidLength = input.nextInt(); //get the input
			if(pyramidLength > 10 || pyramidLength < 0) //check for invalid items
			{
				System.out.println("Invalid input, goodbye.");
				return;
			}
			else
			{
				pyramidLength += 1;
				int spaceLength = pyramidLength;
				int c = 0;
				for(int numrows = 1; numrows < pyramidLength; numrows++) //setup loop
				{
					c = 0;
					while(c < spaceLength) //loop through the number of rows to prepend that - 1 amount of spaces
					{
						System.out.print(" ");
						c++;
					}
					spaceLength -=1;
					for(int i = numrows; i >= 1; i--) //print the numbers in descending order
					{
						System.out.print(i);
						
					}

					System.out.println(""); //add a line break
				}
			}


	}
}