/**
Cody Benkoski
Lab 06
10-10-2018
**/

import java.util.Scanner;

public class PatternB
{
	public static void main(String args[])
	{
		int pyramidLength = 0; //length of pyramid entered by User
		Scanner input = new Scanner(System.in); //scanner class
		String junk;

		System.out.println("Enter an integer 1-10 for the length of the pyramid"); //prompt the user

		while(!input.hasNextInt())
		{
			System.out.println("Invalid input, please try again"); //check for bad input
			junk = input.nextLine();
		}

		
		pyramidLength = input.nextInt();
			if(pyramidLength > 10 || pyramidLength < 0)
			{
				System.out.println("Invalid input, goodbye."); //check for bad input
				return;
			}
			else
			{
				pyramidLength += 1;
			 for(int numrows = pyramidLength; numrows > 0; numrows--) //decrease number of rows to achieve inverted triangle
			 {
			 	for(int i =1; numrows > i; i++) //loop through rows printing the numbers to the right, seperated by space
			 	{
			 		System.out.print(i+" ");
			 	}
			 	System.out.println("");
			 }
			}


	}
}