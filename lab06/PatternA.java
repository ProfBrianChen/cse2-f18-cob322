/**
Cody Benkoski
Lab 06
10-10-2018
**/

import java.util.Scanner;

public class PatternA
{
	public static void main(String args[])
	{
		int pyramidLength = 0; //length of pyramid entered by User
		Scanner input = new Scanner(System.in); //scanner class
		String junk;

		System.out.println("Enter an integer 1-10 for the length of the pyramid"); //prompt the user

		while(!input.hasNextInt()) //if input is invalid, ask again
		{
			System.out.println("Invalid input, please try again");
			junk = input.nextLine();
		}

		
		pyramidLength = input.nextInt();
			if(pyramidLength > 10 || pyramidLength < 0)
			{
				System.out.println("Invalid input, goodbye."); //check for invalid input
				return;
			}
			else
			{
				pyramidLength += 2;
				for(int numrows = 1; numrows < pyramidLength; numrows++) //setup loop
				{
					for(int i = 1; numrows > i; i++) //loop through the number of remaining rows to print out the number in increasing order
					{
						System.out.print(i+" ");
					}
					System.out.println("");
				}
			}


	}
}