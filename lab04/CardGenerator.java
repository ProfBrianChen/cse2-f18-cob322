/** 
Cody Benkoski
9-19-2018
CSE 002
**/
import java.util.Random;

public class CardGenerator //required class definition
{
  public static void main(String args[]) //this program generates a random number, and assings it to a card
  {
  //declare variables
    int randomNumber;
    String cardSuit;
    String cardIdentity;
    //end variables
    Random randGen = new Random();
    
    cardSuit = "";
    cardIdentity = "";
    randomNumber = randGen.nextInt(53) +1;
    
    if(randomNumber >= 1 && randomNumber <= 13) //diamonds
    {
      cardSuit = "Diamonds";
    }
    else if(randomNumber >= 14 && randomNumber <= 26) //clubs
    {
      randomNumber = 27 - randomNumber; //subtract 26 to put the number back in 1 - 13 terms.
      cardSuit = "Clubs";
    }
    else if(randomNumber >= 27 && randomNumber <= 39) //hearts
    {
      randomNumber = 40 - randomNumber; //subtract 39 to put the number back in 1 - 13 terms.
      cardSuit = "Hearts";
    }
    else if(randomNumber >= 40 && randomNumber <= 52) //spades
    {
      randomNumber = 53 - randomNumber; //subtract 52 to put the number back in 1 - 13 terms.
      cardSuit = "Spades";
      
    }
    switch(randomNumber)
      {
        case 13: cardIdentity = "King";
          break;
        case 12: cardIdentity = "Queen";
          break;
        case 11: cardIdentity = "Jack";
          break;
      case 1: cardIdentity = "Ace";
        break;
        default: 
          break;
      }
    if((randomNumber <= 13 && randomNumber >= 11) || randomNumber == 1 )
    {
      System.out.println("You picked the "+cardIdentity+" of "+cardSuit);
    }
    else
    {
    System.out.println("You picked the "+randomNumber+" of "+cardSuit);
    }
    
    
  }
}