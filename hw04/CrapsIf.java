/** 
Cody Benkoski
9-21-2018
CSE 002
**/

//import needed utilities
import java.util.Scanner;
import java.util.Random;
//end import
public class CrapsIf//this program asks the user for a dice pair, and assigns the slang term using if statements.
  //if no user input, it randomly rolls a dice pair
{
  public static void main(String agrs[])
  {
    String userDecision; //contains whether the user selects y or n
    int userPair1; //can be either user provided, or assigned randomly
    int userPair2; //can be either user provided, or assigned randomly
    
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Would you like to enter your own rolls? (answer with y or n): ");
    userDecision = myScanner.next();
    if(userDecision.equals("y")) //if y, prompt user for their number pairs
    {
      System.out.print("Please enter roll one: ");
      userPair1 = myScanner.nextInt(); //get the first roll
      System.out.print("Please enter roll two: ");
      userPair2 = myScanner.nextInt(); //get the second roll
      if(!(userPair1 < 7) || !(userPair2 < 7)) //check to see if user actually entered a valid roll
      {
        System.out.println("Invalid rolls, please try again.");
        return;
      }
    }
    else if(userDecision.equals("n")) //roll for user
    {
      Random myRandom = new Random();
      userPair1 = myRandom.nextInt(6)+1; //assign random rolls
      userPair2 = myRandom.nextInt(6)+1;
      //System.out.print(userPair1);
    }
    else //user entered something that we can't handle
    {
      System.out.println("Invalid input. Please try again");
      return;
    }
      //since errors are returned, and skip execution, we can write an if statement here to assign values
    //(userPair1 == 1 && userPair2 == 1) || (userPair2 == 1 && userPair1 == 1)
    if((userPair1 == 1 && userPair2 == 1))
    {
      System.out.println("You rolled snake eyes");    
    }
    else if((userPair1 == 2 && userPair2 == 2))
    {
       System.out.println("You rolled a hard four");    
    }
    else if((userPair1 == 3 && userPair2 == 3))
    {
      System.out.println("You rolled a hard six");     
    }
    else if((userPair1 == 4 && userPair2 == 4))
    {
       System.out.println("You rolled a hard eight");    
    }
    else if((userPair1 == 5 && userPair2 == 5))
    {
       System.out.println("You rolled a hard ten");    
    }
    else if((userPair1 == 6 && userPair2 == 6))
    { System.out.println("You rolled box cars");    
      
    }
    else if((userPair1 == 2 && userPair2 == 1) || (userPair2 == 2 && userPair1 == 1))
    {
       System.out.println("You rolled an ace deuce");    
    }
    else if((userPair1 == 3 && userPair2 == 1) || (userPair2 == 3 && userPair1 == 1))
    {
       System.out.println("You rolled an easy four");    
    }
    else if((userPair1 == 4 && userPair2 == 1) || (userPair2 == 4 && userPair1 == 1))
    {
      System.out.println("You rolled a fever five");     
    }
    else if((userPair1 == 5 && userPair2 == 1) || (userPair2 == 5 && userPair1 == 1) || (userPair1 == 4 && userPair2 == 2) || (userPair2 == 4 && userPair1 == 2))
    {
      System.out.println("You rolled an easy six");     
    }
    else if((userPair1 == 6 && userPair2 == 1) || (userPair2 == 6 && userPair1 == 1) || (userPair1 == 5 && userPair2 == 2) || (userPair2 == 5 && userPair1 == 2) || (userPair1 == 4 && userPair2 == 3) || (userPair2 == 4 && userPair1 == 3))
    {
      System.out.println("You rolled a seven out");     
    }
    else if((userPair1 == 3 && userPair2 == 2) || (userPair2 == 3 && userPair1 == 2))
    {
      System.out.println("You rolled a fever five");
    }
    else if((userPair1 == 5 && userPair2 == 4) || (userPair2 == 5 && userPair1 == 4) || (userPair1 == 6 && userPair2 == 3) || (userPair2 == 6 && userPair1 == 3))
    {
      System.out.println("You rolled a nine");
    }
    else if((userPair1 == 6 && userPair2 == 5) || (userPair2 == 6 && userPair1 == 5))
    {
              System.out.println("You rolled a yo-leven");
    }
    else if((userPair1 == 6 && userPair2 == 2) || (userPair2 == 6 && userPair1 == 2) || (userPair1 == 5 && userPair2 == 3) || (userPair2 == 5 && userPair1 == 3))
            {
              System.out.println("You rolled an easy eight");
            }
    else if((userPair1 == 6 && userPair2 == 4) || (userPair2 == 6 && userPair1 == 4))
            {
              System.out.println("You rolled an easy ten");
            }
    
  }
}