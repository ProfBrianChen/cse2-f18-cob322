/** 
Cody Benkoski
9-21-2018
CSE 002
**/
//import needed utilities
import java.util.Scanner;
import java.util.Random;
//end import

public class CrapsSwitch //this program asks the user for a dice pair, and assigns the slang term using switch statements.
  //if no user input, it randomly rolls a dice pair
{
  public static void main(String agrs[])
  {
    String switchPlaceholder1; //string used to assign to a dice roll, which when evaluated tells what the roll is
    String switchPlaceholder2; //string used to assign to a dice roll, which when evaluated tells what the roll is
    String combinedHolder; //concatentation of the two placeholders, determines roll
    String userDecision; //contains whether the user selects y or n
    int userPair1; //can be either user provided, or assigned randomly
    int userPair2; //can be either user provided, or assigned randomly
    int modPair1; //calculate modulus, if zero, number 
    int modPair2;
    
     Scanner myScanner = new Scanner( System.in );
    System.out.print("Would you like to enter your own rolls? (answer with y or n): "); //prompt the user for their choice
    userDecision = myScanner.next();
    switchPlaceholder1 = switchPlaceholder2 = ""; //initialize variables
    switch(userDecision)
    {
      case "y": 
      System.out.print("Please enter roll one: ");
      userPair1 = myScanner.nextInt(); //get the first roll
      System.out.print("Please enter roll two: ");
      userPair2 = myScanner.nextInt(); //get the second roll
        
        break;
      case "n":
        Random myRandom = new Random();
      userPair1 = myRandom.nextInt(6)+1; //assign random rolls
      userPair2 = myRandom.nextInt(6)+1;
        break;
      default:
              System.out.println("Invalid input. Please try again");
      return;
        
    }
    //begin value check
          switch(userPair1)
      {
        case 1: switchPlaceholder1 = "a"; break; //a
        case 2: switchPlaceholder1 = "b"; break; //b
        case 3: switchPlaceholder1 = "c"; break; //c
        case 4: switchPlaceholder1 = "d"; break; //d
        case 5: switchPlaceholder1 = "e"; break; //e
        case 6: switchPlaceholder1 = "f"; break; //f
        default: System.out.println("Invalid roll pair!"); return;
      }
              switch(userPair2)
      {
        case 1: switchPlaceholder2 = "A"; break; //A
        case 2: switchPlaceholder2 = "B"; break; //B
        case 3: switchPlaceholder2 = "C"; break; //C
        case 4: switchPlaceholder2 = "D"; break; //D
        case 5: switchPlaceholder2 = "E"; break; //E
        case 6: switchPlaceholder2 = "F"; break; //F
        default: System.out.println("Invalid roll pair!"); return;
      }
    //end value check
combinedHolder = switchPlaceholder1+switchPlaceholder2; //using lowercase and capital as I believe it improves readability, at the expense of some more lines of code
//example output: aA (first letter is ALWAYS lowercase)    
switch(combinedHolder)
    {
        //begin snake eyes permutation
      case "aA": System.out.println("You rolled a snake eyes!"); break;
        // snake eyes
        
        //begin ace deuce
      case "aB": System.out.println("You rolled an ace deuce!"); break;
      case "bA": System.out.println("You rolled an ace deuce!"); break;
        //end ace deuce
        
        //begin easy four
      case "aC": System.out.println("You rolled an easy four!"); break;
      case "cA": System.out.println("You rolled an easy four!"); break;
        //end easy four
        
        //begin fever five
      case "aD": System.out.println("You rolled a fever five!"); break;
      case "dA": System.out.println("You rolled a fever five!"); break;
      case "bC": System.out.println("You rolled a fever five!"); break;
      case "cB": System.out.println("You rolled a fever five!"); break;
        //end fever five
    
    //begin easy six
    case "aE": System.out.println("You rolled an easy six!"); break;
    case "eA": System.out.println("You rolled an easy six!"); break;
    case "bD": System.out.println("You rolled an easy six!"); break;
    case "dB": System.out.println("You rolled an easy six!"); break;
    //end easy six
    
    //begin hard six
    case "cC": System.out.println("You rolled a hard six!"); break;
    //end hard six
    
    //begin seven out
    case "aF": System.out.println("You rolled a seven out!"); break;
    case "fA": System.out.println("You rolled a seven out!"); break;
    case "bE": System.out.println("You rolled a seven out!"); break;
    case "eB": System.out.println("You rolled a seven out!"); break;
    case "cD": System.out.println("You rolled a seven out!"); break;
    case "dC": System.out.println("You rolled a seven out!"); break;
    //end seven out
    
    //begin easy eight
    case "bF": System.out.println("You rolled an easy eight!"); break;
    case "fB": System.out.println("You rolled an easy eight!"); break;
    case "cE": System.out.println("You rolled an easy eight!"); break;
    case "eC": System.out.println("You rolled an easy eight!"); break;
    //end easy eight
    
    //begin hard eight
    case "dD": System.out.println("You rolled a hard eight!"); break;
    //end hard eight
    
    //begin nine
    case "cF": System.out.println("You rolled a nine!"); break;
    case "fC": System.out.println("You rolled an easy eight!"); break;
    case "dE": System.out.println("You rolled an easy eight!"); break;
    case "eD": System.out.println("You rolled an easy eight!"); break;
    //end nine
    
    //begin easy ten
    case "dF": System.out.println("You rolled an easy ten!"); break;
    case "fD": System.out.println("You rolled an easy ten!"); break;
    //end easy ten
    
    //begin hard ten
    case "eE": System.out.println("You rolled a hard ten!"); break;
    //end hard ten
    
    //begin yo leven
    case "eF": System.out.println("You rolled a yo leven!"); break;
    case "fE": System.out.println("You rolled a yo leven!"); break;
    //end yo leven
    
    //begin box cars
  case "fF": System.out.println("You rolled boxcars!"); break;
    //end box cars
    
    }
    
  }
}