/** 
Cody Benkoski
9-12-2018
CSE 002
**/
import java.util.Scanner;

//calculates cost per person of a check split n ways where n is the number of people.

public class Check  
{ //begin main class
  public static void main(String args[])
  { //begin main method
    
    //define needed variables
    double totalCost, costPerPerson;
    int dollars, dimes, pennies;
    //end variable definition 
    
    //prompt for, and assign values
    Scanner myScanner = new Scanner( System.in ); //construct scanner object
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt the user for original check cost
    double checkCostOriginal = myScanner.nextDouble(); //get the input (original check cost)
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompt for the tip as a whole number
    double tipPercent = (myScanner.nextDouble())/ 100; //get the tip, then divide by 100 to get the percentage
    
    System.out.print("Enter the number of people who went out to dinner: "); //prompt for n number of people at the dinner
    int numPeople = myScanner.nextInt(); //get the number of people
    //end prompts and value assigning
    
    //calculate total cost per person
    
    totalCost = checkCostOriginal * (1 + tipPercent); //multiply original check cost by the tip +1 (prevents multiple operations)
    costPerPerson = totalCost / numPeople; //divide total cost by n people (drops decimals)
    dollars = (int)costPerPerson; //drop all decimals to get dollar amount
    dimes = (int)(costPerPerson * 10) % 10; //get remaineder in tens place
    pennies = (int)(costPerPerson * 100) % 10; //get remaineder in ones place
    
    //end calculations
    
    //print out final cost per person
    System.out.println("Each person in the group owes: $" + dollars +"."+ dimes + pennies); //print final cost per person
    
    

    
    

    
    
    
  } //end main method
} //end main class
