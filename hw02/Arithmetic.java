/** 
Cody Benkoski
9-06-2018
CSE 002
**/
public class Arithmetic
{
  public static void main(String args[])
  {
    //given data
    int numPants = 3; //number of pants purchased.
    
    double pantsPrice = 34.98; //purchase price per 1 pants.double
    
    int numShirts = 2; //number of sweatshirts purchased.
    
    double shirtPrice = 24.99; //purchase price per 1 sweatshirt
    
    int numBelts = 1; //number of belts purchased
    
    double beltPrice = 33.99; //purchase price per 1 beltCost
    
    double paSalesTax = 0.06; //sales tax in PA expressed as decimal.double
    //end given data
    
    //variables used for storing total costs
    double totalCostOfPants; //total cost of pants
    
    double totalCostOfShirts; //total cost of sweatshirts
    
    double totalCostOfBelts; //total cost of belts
    
    double totalCostItemsNoTax; //total cost of all items with no tax
    
    double totalCostItemsTax; //total cost of all items with tax
    
    double totalSalesTax; //total sales tax levied on all items
    
    double salesTaxPants;
    
    double salesTaxShirts;
    
    double salesTaxBelts;
    
    //end total cost variables
    
    
    //compute total cost of items
    //general form: = totalCostItem = itemPrice*numOfItems
    
    totalCostOfPants = pantsPrice * numPants; //compute total cost of pants
    
    totalCostOfShirts = shirtPrice * numShirts; //compute total cost of shirts
    
    totalCostOfBelts = beltPrice * numBelts; //compute total cost of belts
    
    //end total price computations
    
    //compute sales tax given total cost
    //general form: salesTaxItem = totalCostItem*paSalesTax
    
    salesTaxShirts = totalCostOfShirts*paSalesTax; //compute sales tax on shirts
    
    salesTaxPants = totalCostOfPants*paSalesTax; //compute sales tax on pants
    
    salesTaxBelts = totalCostOfBelts*paSalesTax; //compute sales tax on belts
    
    //end sales tax computations
    
    //begin entire purchase total calculations
    //general form: varies, generally a sum of all item costs
    
    totalSalesTax = salesTaxShirts + salesTaxPants + salesTaxBelts; //sum all sales tax to get total tax
    
    totalCostItemsNoTax = totalCostOfShirts + totalCostOfPants + totalCostOfBelts; //sum all total costs of items
    
    totalCostItemsTax = totalCostItemsNoTax + totalSalesTax; //sum of total cost and total sales tax
    
    //end entire purchase calculations
    
    //remove fractional pennies
    //move decimal over two places (multiply by 100), we convert to int, hence lopping off the remaing decimal points
    //with that, we then divide by a 100.0 (a double), to shift it over two places and preserve what lies to the right of the points
    //general form: costWithFractionalPennies = (integer cast)(costWithFractionalPennies * 100) / 100.0, where 100.0 is a double.
    
    salesTaxShirts = (int)(salesTaxShirts * 100) / 100.0; 
    
    salesTaxBelts = (int)(salesTaxBelts * 100) / 100.0;
    
    salesTaxPants = (int)(salesTaxPants * 100) / 100.0; 
    
    totalSalesTax = (int)(totalSalesTax * 100) / 100.0;
    
    //total cost items (no tax) is not included as it does not extend into the thousandths place.
    
    totalCostItemsTax = (int)(totalCostItemsTax * 100) / 100.0;
    
    //end decimal shifting
    
    //begin printing variables
    
    //print total cost of individual items
    System.out.println("Total cost of belts: $"+totalCostOfBelts);
    System.out.println("Total cost of pants: $"+totalCostOfPants);
    System.out.println("Total cost of sweatshirts: $"+totalCostOfShirts+"\n");
    //end total cost individual print
    
    //print individual sales tax levied on items
    System.out.println("Total sales tax on belts: $"+salesTaxBelts);
    System.out.println("Total sales tax on pants: $"+salesTaxPants);
    System.out.println("Total sales tax on sweatshirts: $"+salesTaxShirts+"\n");
    //end individual sales tax print
    
    //being total combined cost print
    System.out.println("Total cost of items (no tax): $"+totalCostItemsNoTax);
    System.out.println("Total sales tax levied on items: $"+totalSalesTax);
    System.out.println("Total cost of items: $"+totalCostItemsTax);
    //end total cost print
    
    //end printing variables
    
  }
}