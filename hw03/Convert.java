/** 
Cody Benkoski
9-14-2018
CSE 002
**/

import java.util.Scanner;
//23523.23 * 45 * 9.08169e-13 * 27,154
//        gallons to cubic miles (9)
// one inch of rain falling on one acre 27,154

//feet (rain) per acre = 0.00015941688
public class Convert //find amount of rainfall (in cubic miles) given acres and rainfall (in inches)
{ //begin main class
  public static void main(String args[])
  { //begin main method
    
    //define needed variables
    double affectedAcres;
    double averageRainfall;
    final int SQ_FEET_IN_ACRE = 43560;
    final double IN_IN_FOOT = (1.0/12.0);
    double AffectedSqFt;
    double averageRainfallFt;
    double cubicFtRain;
    double cubicMileRain;
    //end variable definitions
    
    Scanner myScanner = new Scanner( System.in ); //construct scanner object
    
    //gprompt, and get user input
    System.out.print("Enter the affected acres: ");
    affectedAcres = myScanner.nextDouble(); //get the affected acres from user
    
    System.out.print("Enter the average rainfall: ");
    averageRainfall = myScanner.nextDouble(); //get the rainfall amount
    
    //end user input
    
    //begin calculations
    
    //perform needed conversions
    AffectedSqFt = SQ_FEET_IN_ACRE * affectedAcres; //convert acres into sq feet using constant (found using Google)
    averageRainfallFt = averageRainfall * IN_IN_FOOT; //convert rainfall in inches, to feet, by dividing by 12
    //end conversion
    
    //find cubic feet 
    //since we now have the square feet of the affected area, we will now multiply that by the feet of rain that have fallen
    // affectedFt^3 = Affectedft^2 * rainfallft^1;
    cubicFtRain = AffectedSqFt * averageRainfallFt;
    //end calculations
    
    //convert cubic feet to cubic miles
    cubicMileRain = cubicFtRain / (double)(Math.pow(5280, 3)); //convert feet in mile to cubic feet (raise by 3)
    //end conversion
    
    //print out conversion
    System.out.println(cubicMileRain +" cubic miles");
    //end print out
    
  } //end main method
} //end main class