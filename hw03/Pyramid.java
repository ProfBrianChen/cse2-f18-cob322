/** 
Cody Benkoski
9-14-2018
CSE 002
**/

import java.util.Scanner;

public class Pyramid //program which takes user input for the square length of a traingular pyramid, and height and finds volume
{ //begin main class
  public static void main(String args[])
  { //begin main method
    
    //define needed variables
    double PyramidSquare;
    double PyramidLength;
    double PyramidVolume;
    //end variable defintions
    
    Scanner myScanner = new Scanner( System.in ); //construct scanner object
    
    //prompt, and get user input
    System.out.print("Enter Square Height of Pyramid: ");
    PyramidSquare = myScanner.nextDouble(); //get the square height
    
    System.out.print("Enter Length of a side of the Pyramid: ");
    PyramidLength = myScanner.nextDouble(); //pyramid length
    //end user input
    
    //calculations
    //calculate volume
    //general form V = (squareLength)(squareLength)(height)*(1/3)
    PyramidVolume = (Math.pow(PyramidSquare, 2))*(PyramidLength)*(1.0/3.0);
    //end calculations
    
    //print out volume
    System.out.println(PyramidVolume);
    //end print
    
    
  } //end main method
} //end main class