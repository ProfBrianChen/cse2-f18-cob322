/** 
Cody Benkoski
10-25-2018
CSE 002
**/
import java.util.Scanner;

public class hw07
{
	public static double checkForBadInputChar(Scanner input)
	{
		return (double)0;
	}
	public static String sampleText(Scanner input) //prompts user for string, and returns it
	{
		String userString = "";
		System.out.println("Please enter a string to be manipulated: ");
		userString = input.nextLine();
		System.out.println("You entered: "+userString);
		return userString; //return the variable so it can be used in other functions
	}
	public static int getNumOfNonWSCharacters(String userString)
	{
		String noWS = replaceGeneral(userString, ' ', "");
		return noWS.length();
	}
	public static int getNumOfWords(String userString) 
	{
		char replacee = ' ';
		int numWords = 0;
		String singleSpace = shortenSpace(userString);
		int strlen = singleSpace.length();
		char currChar = 'a';
		for(int i = 0; i < strlen; i++)
		{
			currChar = userString.charAt(i);
			if(currChar == replacee)
			{
				numWords +=1;
			}
		}
		return numWords+1; //account for beginning word.
	}
	public static int findText(String needle, String haystack)
	{
		String runon;
		runon = replaceGeneral(shortenSpace(haystack), ' ', ""); //make the string one long string with no whitespaces, assuming we're looking for a string, in the purest sense (a collection of non WS chars)
		int nlen = needle.length(); //get length of needle, so we can search through this X groupings of chars
		int rlen = runon.length();
		int howManySearches = (int)Math.ceil((double)rlen/nlen);
		int matches =0;
		int matchesReal = 0;
		int nlenPrev =0;
		char matchPrev = 'a';
		char toBeReplc = 'a';
		if(nlen != 1) //if its greater than one, subtract one to account for a missing char at the end.
		{
			nlen -= 1;
		}
		for(int i = 0; i <= nlen; i++)
		{
			toBeReplc = needle.charAt(i);
			runon = replaceGeneral(runon, toBeReplc, " "); //replace every matching char with a space so we can count them
		}
		int newLength = runon.length();
		for(int x =0; x < newLength; x++) //count the number of spaces, divide by needle length, take the ceiling
		{
			if(runon.charAt(x) == ' ')
			{
				matches +=1;
				if(matches == nlen)
				{
					matchesReal += 1;
					matches = 0;
				}
			}
			else
			{
				matches = 0; //reset the counter once we don't hit a match
			}
		}
		return matchesReal;
	}
	public static String replaceGeneral(String userString, char replacee, String replacement) //this is char bc we are comparing chars, replacement can be a string since its being appended to a string.
	{
		String frankenString = ""; //I'm assuming we can't use the replace function, so we'll loop through the string and if the character matches, we append that to a new string, if it doesn't we append the new character
		int strlen = userString.length();
		char currChar = 'a';
		for(int i = 0; i < strlen; i++)
		{
			currChar = userString.charAt(i);
			if(currChar == replacee)
			{
				frankenString += replacement;
			}
			else
			{
				frankenString += ""+currChar; //implictly cast currChar to a string
			}
		}
		return frankenString;
	}
	public static String shortenSpace(String userString)
	{
		char replacee = ' ';
		String replacement = " ";
		String frankenString = ""; //I'm assuming we can't use the replace function, so we'll loop through the string and if the character matches, we append that to a new string, if it doesn't we append the new character
		int strlen = userString.length();
		char currChar = 'a';
		char currCharNext = 'a';
		int oneAbove = 0;
		for(int i = 0; i < strlen; i++)
		{
			currChar = userString.charAt(i);
			if((i+1) == strlen)
			{
				continue;
			}
			else
			{
				oneAbove = i+1;
			}
			currCharNext = userString.charAt(oneAbove);
			if(currChar == ' ' && currCharNext == ' ')
			{
				frankenString +=""; //append nothing, since the next char is a space
			}
			else
			{
				frankenString += ""+currCharNext; //add the next char to the string, as its a space

			}
		}
		return frankenString;
	}
	public static String promptUserForSearch(Scanner input)
	{
		System.out.println("Enter the string you would like to search for: ");
		return input.next();
	}
	public static void printMenu()
	{
		System.out.println("c - Number of whitespace characters.");
		System.out.println("w - Number of words.");
		System.out.println("f - Find text.");
		System.out.println("r- Replace all !'s .");
		System.out.println("s - Shorten spaces.");
		System.out.println("q - Quit.");
		System.out.println("p - Print out my string again.");
	}
	public static void main(String args[])
	{
		String userChoice = "q";
		Scanner input = new Scanner(System.in);
		String userString = "a";
		do
		{
			userString = sampleText(input);
			System.out.println("Your string is empty!");
		}
		while(userString.length() == 0);

		do
		{
			printMenu();
			userChoice = input.next();
			switch(userChoice)
			{
				case "q": System.out.println("Goodbye."); 
				System.exit(0); break;
				case "c": System.out.println("Number of non whitespace characters: "+getNumOfNonWSCharacters(userString)); break;
				case "w": System.out.println("Number of words: "+getNumOfWords(userString)); break;
				case "f": String needle = promptUserForSearch(input);
				System.out.println("\""+needle+"\" instances: "+findText(needle, userString)); break;
				case "r": System.out.println("Edited text: "+replaceGeneral(userString, '!', ".")); break;
				case "s": System.out.println("Edited text: "+shortenSpace(userString)); break;
				case "p": System.out.println(userString);
			}
		}
		while(userChoice != "q");
	}
}