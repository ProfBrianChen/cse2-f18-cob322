/** 
Cody Benkoski
10-18-2018
CSE 002
**/
import java.util.Scanner; //import scanner
public class EncryptedX //display a letter X as a pattern of asterisks
{
	public static void main(String args[])
	{
		String junk = ""; //used to clear buffer
		int length;
		Scanner input = new Scanner(System.in); //create a new scanner class

		System.out.println("Enter the length of the X: ");
		//BEGIN ERROR CHECKING
		while(!input.hasNextInt())
		{
			System.out.println("Invalid input, please try again.");
			junk = input.next();
		}
		length = input.nextInt();

		if(length == 0 || length < 0 || length > 100) //can't have a negative length, or 0 length
		{
			System.out.println("Invalid input, please try again.");
			return;
		}
		//END ERROR CHECKING
		//DEFINE NEEDED VARIABLES
		int auxiliaryAsterisksBegin = 0;
		int auxiliaryAsterisksEnd = 0;
		int mainAsterisks = 0;
		int begin = length;
		int end = length;
		int midpoint = length/2;
		//END VARIABLES
		//BEGIN TOP X
		for(int i = 1; i <= midpoint; i++) //this loop handles the top of the X
		{

			auxiliaryAsterisksBegin = i-1; //aux.  asterisks begin at -1 the counter as they are not used in first row
			auxiliaryAsterisksEnd = i-1;
			mainAsterisks = length - (2*i); //acount for other asterisks on sides (auxiliary ones)
			//BEGIN TOP OF X
			while(auxiliaryAsterisksBegin != -1) //-1 so we can add a space once it hits 0
			{
				if(auxiliaryAsterisksBegin > midpoint)
				{
					break; //break out once we hit a midpoint so we can print an asterisk
				}
				else if(auxiliaryAsterisksBegin == 0)
				{
					System.out.print(" ");
					break;
				}
				System.out.print("*");
				auxiliaryAsterisksBegin--; //subtract from counter
			}
			while(mainAsterisks >= 0)
			{
				System.out.print("*");
				mainAsterisks--;
			}
			System.out.print(" ");
			while(auxiliaryAsterisksEnd != -1) //-1 so we can add a space once it hits 0
			{
				if(auxiliaryAsterisksEnd > midpoint)
				{
					break;
				}
				else if(auxiliaryAsterisksEnd == 0)
				{
					System.out.print(" ");
					break;
				}
				System.out.print("*");
				auxiliaryAsterisksEnd--;
			}
			System.out.println(""); //add a line break between completed lines
		}
			//END TOP OF X
			//BEGIN MID-SECTION
			//this handles the mid section outside of the main for loops
			int middleAsterisks = midpoint;
			while(middleAsterisks != 0) //print the leading asterisks
			{
				System.out.print("*");
				middleAsterisks--;
			}
			if(length % 2 == 0)
			{
				System.out.print(" "); //print middle space	
			}
			else
			{
				System.out.print("  "); //print middle space twice if its odd, makes it look nicer
			}							//I guess we could maybe print an asterisk here, but I'll stick with a space for now
			
			middleAsterisks = midpoint;
			while(middleAsterisks != 0) //print trailing asterisks
			{
				System.out.print("*");
				middleAsterisks--;
			}
			System.out.println(""); //add line seperator
			//END MID-SECTION
			//BEGIN BOTTOM OF X
			//This loop is identical to the one above, just that it counts down.
			//For this, we need it to begin at one below the midsection, so we count down instead of up
		for(int u = midpoint; u > 0; u--) //this loop handles the bottom of the X
		{

			auxiliaryAsterisksBegin = u-1; //aux.  asterisks begin at -1 the counter as they are not used in first row
			auxiliaryAsterisksEnd = u-1;
			mainAsterisks = length - (2*u); //acount for other asterisks on sides (auxiliary ones)
			//BEGIN TOP OF X
			while(auxiliaryAsterisksBegin != -1) //-1 so we can add a space once it hits 0
			{
				if(auxiliaryAsterisksBegin > midpoint)
				{
					break; //break out of loop once we hit a midpoint
				}
				else if(auxiliaryAsterisksBegin == 0)
				{
					System.out.print(" ");
					break;
				}
				System.out.print("*");
				auxiliaryAsterisksBegin--;
			}
			while(mainAsterisks >= 0)
			{
				System.out.print("*");
				mainAsterisks--;
			}
			System.out.print(" ");
			while(auxiliaryAsterisksEnd != -1) //-1 so we can add a space once it hits 0
			{
				if(auxiliaryAsterisksEnd > midpoint)
				{
					break;
				}
				else if(auxiliaryAsterisksEnd == 0)
				{
					System.out.print(" ");
					break;
				}
				System.out.print("*");
				auxiliaryAsterisksEnd--;
			}
			System.out.println("");

		}
	}
}