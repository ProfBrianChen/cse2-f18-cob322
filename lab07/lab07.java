/**
Cody Benkoski
Lab 07
10-24-2018
**/

import java.util.Random;
import java.util.Scanner;

public class lab07
{
	public static int generateRandom()
	{
		Random randomGenerator = new Random();
		return randomGenerator.nextInt(10);
	}
	public static String adjective()
	{
		String output = "";
		switch(generateRandom())
		{
			case 0: output = "yellow"; break;
			case 1: output = "fast"; break;
			case 2: output = "slow"; break;
			case 3: output = "big"; break;
			case 4: output = "small"; break;
			case 5: output = "brown"; break;
			case 6: output = "tall"; break;
			case 7: output = "wide"; break;
			case 8: output = "long" ;break;
			case 9: output = "loud"; break;
			default: break;
		}
		return output;
	}
	public static String subject()
	{
		String output =  "";
		switch(generateRandom())
		{
			case 0: output = "dog"; break;
			case 1: output = "person"; break;
			case 2: output = "cat"; break;
			case 3: output = "professor"; break;
			case 4: output = "girl"; break;
			case 5: output = "boy"; break;
			case 6: output = "hamster"; break;
			case 7: output = "squirrel"; break;
			case 8: output = "deer"; break;
			case 9: output = "bear"; break;
			default: break;
		}
		return output;
	}
	public static String verbPast()
	{
		String output ="";
		switch(generateRandom())
		{
			case 0: output = "ran"; break;
			case 1: output = "jumped"; break;
			case 2: output = "walked"; break;
			case 3: output = "fainted"; break;
			case 4: output = "slept"; break;
			case 5: output = "looked"; break;
			case 6: output = "talked"; break;
			case 7: output = "saw"; break;
			case 8: output = "ate"; break;
			case 9: output = "fell"; break;
			default: break;
		}
		return output;
	}
	public static String object()
	{
		String output = "";
		switch(generateRandom())
		{
			case 0: output = "fox"; break;
			case 1: output = "worker"; break;
			case 2: output = "programmer"; break;
			case 3: output = "child"; break;
			case 4: output = "man"; break;
			case 5: output = "woman"; break;
			case 6: output = "bus driver";break;
			case 7: output = "chinchilla"; break;
			case 8: output = "chipmunk"; break;
			case 9: output = "bird"; break;
			default: break;
		}
		return output;
	}

	public static String topicSentence()
	{
		String subject = subject();
		System.out.println("The "+adjective()+" "+subject+" "+verbPast()+" the "+adjective()+" "+object()+".");
		return subject;
	}
	public static void actionSentence(String subject, int numTimes)
	{
		subject = "This "+subject;
		String connector = " was ";
		String connector2 = " when ";
		if(generateRandom() > 5)
		{
			subject = "It";
		}
		if(generateRandom() < 6) //generate a new number
		{
			connector = " did ";
		}
		if(generateRandom() > 3) //new number
		{
			connector2 = " before ";
		}
		for(int i = 0; i < numTimes; i++)
		{

			System.out.println(subject+connector+verbPast()+connector2+adjective()+" "+object()+".");
		}
	}
	public static void conclusion(String subject)
	{
		String end = " alone.";
		if(generateRandom() > 8)
		{
			end = " until tomorrow.";
		}
		System.out.print("The "+subject+" left the "+object()+end);
	}		
	public static void main(String args[])
	{
		Scanner input = new Scanner( System.in);
		String subject = topicSentence();
		int userChoice;
		System.out.println("Would you like to contine? Type how many more sentences you want (0 to quit): ");
		while(!input.hasNextInt())
		{
			System.out.println("invalid input, try again.");
			input.next();
		}
		userChoice = input.nextInt();
		if(userChoice == 0)
		{
			System.out.println("Goodbye!");
			System.exit(0);
		}
		else if(userChoice > 50)
		{
			System.out.println("Input too large");
			System.exit(0);
		}
		else
		{
			actionSentence(subject, userChoice);
		}
		conclusion(subject);


	}
}