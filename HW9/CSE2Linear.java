/**
Cody Benkoski
HW09
CSE 2
**/

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear
{
	public static void main(String args[])
	{
		Scanner input = new Scanner(System.in);
		int[] StudentGrades = new int[15];
		int iterations = 0;
		System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
		iterations = 0;
		assignGrades(iterations, StudentGrades, input);
		System.out.print("Enter a grade to search for (binary): ");
		checkInvalid(input);
		int value = input.nextInt();
		int midpoint = StudentGrades.length/2;
		int gradeSearch = searchForGradeB(input, StudentGrades, value, StudentGrades.length-1, 0);
		if( gradeSearch < 0)
			System.out.println("The value "+value+" was not found in the list after "+gradeSearch*-1+" iterations"); //transform the neg to a postive number
		else
			System.out.println("The value "+value+" was found in the list after "+gradeSearch+" iterations");
		System.out.println("Scrambled: ");
		scrambleArray(StudentGrades);
		printArray(StudentGrades);
		System.out.print("Enter a grade to search for (linear): ");
		checkInvalid(input);
		value = input.nextInt();
		gradeSearch = searchForGradeL(input, StudentGrades, value);
		if( gradeSearch < 0)
			System.out.println("The value "+value+" was not found in the list after "+gradeSearch*-1+" iterations"); //transform the neg to a postive number
		else
			System.out.println("The value "+value+" was found in the list after "+gradeSearch+" iterations");


	}
	public static void checkInvalid(Scanner input)
	{
				while(!input.hasNextInt())
		{
			System.out.println("That's an invalid grade.");
			String junk = input.nextLine();
		}
	}
	public static boolean checkBounds(int grade)
	{
		if(grade > 100 || grade < 0)
			return true;
		else
			return false;
	}
	public static void scrambleArray(int[] array)
	{
		int tempSwitch = 0;
		int numSwap = 30;
		int index1 = 0;
		int index2 = 1;
		int length = array.length -1;
		for (int i = 0; i < numSwap ;i++ ) 
		{
			index1 = (int)(Math.random() * ((length) + 1));	 //generate random numbers
			index2 = (int)(Math.random() * ((length) + 1));	
			//System.out.println(index1 +" "+index2);
			tempSwitch = array[index1]; //give them a switch
			array[index1] = array[index2];
			array[index2] = tempSwitch;
		}
	}

	public static void assignGrades(int iterations, int[] StudentGrades, Scanner input)
	{
		int grade =0;
		while(!input.hasNextInt())
		{
			System.out.println("Invalid grade entered. Try again.");
			String junk = input.nextLine();
		}
		while(input.hasNextInt() && iterations <= 14)
		{

			grade = input.nextInt();
			if(checkBounds(grade))
			{
				System.out.println("You've entered a number outside the bounds.");
				System.out.println("Grades remaining: "+(15-iterations));
				continue;	
			}
			if(iterations == 0)
			{
				StudentGrades[0] = grade;
				System.out.println(iterations+" "+grade);
				iterations++;
				continue;
			}
			else if(StudentGrades[iterations-1] <= grade)
			{
				StudentGrades[iterations] = grade;
				System.out.println(iterations+" "+grade);
				if(iterations == 14)
					return;
				iterations++;
				continue;
			}
			else
			{
				System.out.println("You must enter grades in ascending order.");
				System.out.println("Grades remaining: "+(15-iterations));
			}
			System.out.println(iterations);
		}
	
	}
	public static int searchForGradeB(Scanner input, int[] grades, int value, int high, int low)
	{
		int iterations = 0;
		while (low <= high) 
		{
			iterations++;
	        int midpoint = (low + high)/2;
	        if(grades[midpoint] == value) 
	            return iterations; //if its found, return the iterations right away, ignoring the rest of the loop
	        else if(grades[midpoint] > value)  //if its below the current midpoint, set the current to the high and iterate below
	            high = midpoint-1; 
	        else if(grades[midpoint] < value)  // if its above, set the curr midpoint to a low and iterate above it.
	            low = midpoint+1;
    	}
    	return iterations*-1; //return a negative number of iterations stating that it was not found
	}
	public static void printArray(int[] cards)
	{
		System.out.println();
		for (int index = 0; index < cards.length;index++ ) 
		{
			System.out.print(cards[index]+" ");	
		}
		System.out.println();
	}

	public static int searchForGradeL(Scanner input, int[] array, int value)
	{
		int length = array.length;
		int index = 0;
		int iterations = 1;
		while(index <= length) //go through the array, until our index reaches the length
		{
			if(array[index] == value) //check if the array value is our desired value
				return iterations; //return the number of iterations if it is
			iterations++;
			index++; //increase the index as we move through the array
		}
		return iterations*-1; //negative iteration means we didn't find the value
	}
}