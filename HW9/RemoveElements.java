/**
Cody Benkoski
HW09
CSE 2
**/

import java.util.Scanner;
public class RemoveElements
{
	public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){ //bad Erlang tuple memories
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }

public static int[] remove(int[] num, int target)
{
	int oldArrCounter = 0;
	int targetCount = 0;
	for (int i = 0; i < num.length;i++) //count how many times the target occurs
	{
		if(num[i] == target)
			targetCount++;
	}
	int newArrSize = (num.length-1) - targetCount;
	int[] newArr = new int[newArrSize]; //use how many times the target occurs to create a new array
	for(int y = 0; y < newArr.length; y++)
	{
		if(num[y] == target) //if we hit the value creating a new array, increase the old array's index and continue, avoiding inserting that value into the new loop
		{
			oldArrCounter++;
			continue;
		}
		newArr[y] = num[oldArrCounter]; //if we didn't hit the target, proceed normally and add the values
		oldArrCounter++;

	}
	return newArr;
}
public static int[] delete(int[] num, int index)
{
	int newArr[] = new int[9];
	int i = 0;
	while(i < 9)
	{
		if(i >= index)
			newArr[i] = num[i+1]; //once we hit the value we want to delete, increase the other array's index by one to account for it not existing
		else
			newArr[i] = num[i]; //if we haven't hit the val yet, proceed normally
		i++;

	}
	return newArr;
}

public static int[] randomInput()
{
	int rand[] = new int[10];
	for(int i =0; i < 10; i++)
	{

	 rand[i]=(int)(Math.random() * 10);	 //generate random numbers
	}
	return rand; //return them in an array
}
}