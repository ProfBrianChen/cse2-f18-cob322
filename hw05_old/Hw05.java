/**
Cody Benkoski
HW 05
10-7-18
**/

import java.util.Scanner; //import needed libraries
import java.util.Random;
import java.text.DecimalFormat;

public class Hw05
{
	public static void main(String args[]) //used to generate 5 draw poker, and figure out the probability of specific hands occuring
	{

		int numGen; //stores number of hands should be generate

		int num4k = 0;
		int num3k = 0;
		int num2p = 0;
		int num1p = 0;

		String junk; //variable to clear out buffer
		Scanner scnr = new Scanner(System.in);

		System.out.println("Please enter the number of hands to generate: ");
		while(!scnr.hasNextInt())
		{
			System.out.println("Invalid input, please try again.");
			junk = scnr.nextLine();
		}

		numGen = scnr.nextInt();

		int r = 0;
			//card with both ident + suit
			String card1 = "";
			String card2 = "";
			String card3 = "";
			String card4 = "";
			String card5 = "";
			//card with only ident
			String card1I = "";
			String card2I = "";
			String card3I = "";
			String card4I = "";
			String card5I = "";
			//card with only suit
			String card1S = "";
			String card2S = "";
			String card3S = "";
			String card4S = "";
			String card5S = "";

			    String cardSuit = "";
			    String cardIdentity = "";
			    int randomNumber = 0;
		while(r < numGen)
		{

			int c = 0;
			while(c <= 5)
			{
				Random randGen = new Random();

			    randomNumber = randGen.nextInt(53) +1;
			    
			    if(randomNumber >= 1 && randomNumber <= 13) //diamonds
			    {
			      cardSuit = "D";
			    }
			    else if(randomNumber >= 14 && randomNumber <= 26) //clubs
			    {
			      randomNumber = 27 - randomNumber; //subtract 26 to put the number back in 1 - 13 terms.
			      cardSuit = "C";
			    }
			    else if(randomNumber >= 27 && randomNumber <= 39) //hearts
			    {
			      randomNumber = 40 - randomNumber; //subtract 39 to put the number back in 1 - 13 terms.
			      cardSuit = "H";
			    }
			    else if(randomNumber >= 40 && randomNumber <= 52) //spades
			    {
			      randomNumber = 53 - randomNumber; //subtract 52 to put the number back in 1 - 13 terms.
			      cardSuit = "S";
			      
			    }
			    switch(randomNumber)
			      {
			        case 13: cardIdentity = "K";
			          break;
			        case 12: cardIdentity = "Q";
			          break;
			        case 11: cardIdentity = "J";
			          break;
			      case 1: cardIdentity = "A";
			        break;
			        default: 
			          break;
			      }
			    if((randomNumber <= 13 && randomNumber >= 11) || randomNumber == 1 ) //after this, an ace of spades is represented as AS for easier checking
			    {
			    	switch(c)
			    	{
			    		case 1: card1 = cardIdentity+cardSuit; //define the card identity and the card itself
			    				card1S = cardSuit; 				//used to determine if the face value is equal, or if two cards are identical
			    				card1I = cardIdentity;
			    				break;
			    		case 2: card2 = cardIdentity+cardSuit; 
			    				card2S = cardSuit;
			    				card2I = cardIdentity;
			    				break;
			    		case 3: card3 = cardIdentity+cardSuit; 
			    				card3S = cardSuit;
			    				card3I = cardIdentity;
			    				break;
			    		case 4: card4 = cardIdentity+cardSuit; 
			    				card4S = cardSuit;
			    				card4I = cardIdentity;
			    				break;
			    		case 5: card5 = cardIdentity+cardSuit; 
			    				card5S = cardSuit;
			    				card5I = cardIdentity;
			    				break;
			    		default: break;

			    	}


			    }
			    else
			    {
					switch(c)
			    	{
			    		case 1: card1 = randomNumber+cardSuit; //add the face value to card, then break apart the suit and identity
			    				card1S = cardSuit;
			    				card1I = Integer.toString(randomNumber);
			    				break;
			    		case 2: card2 = randomNumber+cardSuit; 
			    				card2S = cardSuit;
			    				card2I = Integer.toString(randomNumber);
			    				break;
			    		case 3: card3 = randomNumber+cardSuit; 
			    				card3S = cardSuit;
			    				card3I = Integer.toString(randomNumber);
			    				break;
			    		case 4: card4 = randomNumber+cardSuit; 
			    				card4S = cardSuit;
			    				card4I = Integer.toString(randomNumber);
			    				break;
			    		case 5: card5 = randomNumber+cardSuit; 
			    				card5S = cardSuit;
			    				card5I = Integer.toString(randomNumber);
			    				break;
			    		default: break;

			    	}
			    }
			    if(c == 1 && card1 == card2)
			    {
			    	c -= 1; //subtract one to run another generate since its the same card
			    }
			    else if(c == 2 && card2 == card3)
			    {
			    	c -= 1;
			    }
			    else if(c == 3 && card3 == card4)
			    {
			    	c -= 1;
			    }
			    else if(c == 4 && card4 == card5)
			    {
			    	c -= 1;
			    }
				c++;
			}

			if((card1I == card2I && card2I == card3I && card3I == card4I)
				|| (card1I == card2I && card2I == card3I && card3I == card5I)
				||(card1I == card2I && card2I == card3I && card3I == card4I)
				||(card2I == card3I && card3I == card4I && card4I == card5I)) //check if four of a kind
			{
				num4k += 1;
			}
			else if((card1I == card2I && card2I == card3I) //check if 3 of a kind
				|| (card1I == card2I && card2I == card4I)
				||(card1I == card2I && card2I == card5I)
				||(card2I == card3I && card3I == card5I))
			{
				num3k +=1;
			}
			else if((card1I == card2I && card3I == card4I) //check if two pair
				|| (card1I == card2I && card4I == card5I)
				||(card3I == card4I && card2I == card5I)
				||(card1I == card5I && card4I == card5I))
			{
				num2p +=1;
			}
			else if((card1I == card2I) //check if one pair
				|| (card3I == card4I)
				||(card4I == card5I)
				||(card1I == card5I)
				||(card3I == card5I)
				||(card1I == card4I)
				||(card2I == card5I)
				||(card2I == card3I)
				||(card2I == card4I))
			{
				num1p +=1;
			}
			r++;
		}

		//print out information to user
		DecimalFormat decimal = new DecimalFormat("##.####"); 
		//only two numbers to left of decimal as our probailities *shouldn't* go that high
		System.out.println("Number of user loops: "+numGen);
		System.out.println("Probability of four of a kind: "+decimal.format((num4k/(double)numGen)*100.0));
		System.out.println("Probability of three of a kind: "+decimal.format((num3k/(double)numGen)*100.0));
		System.out.println("Probability of two pairs: "+decimal.format((num2p/(double)numGen)*100.0));
		System.out.println("Probability of one pairs "+decimal.format((num1p/(double)numGen)*100.0));


	}
}