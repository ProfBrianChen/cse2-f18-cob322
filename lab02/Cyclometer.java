/** 
Cody Benkoski
9-05-2018
CSE 002
**/

//Using time and rotation counts as an input, this class calculates the distance (miles) of a bicycle trip
//Also displayed are the inputs of time (seconds) and rotations 
public class Cyclometer //main class begin
{
  public static void main(String args[]) //main method begin
  {
    //begin integer declarations
    int secsTrip1 = 480; //seconds traveled in trip 1
    int secsTrip2 = 3220; //seconds traveled in trip 2
    int countsTrip1 = 1561; //number of rotations during trip 1561
    int countsTrip2 = 9037; //number of rotations in trip 2
    //end integer declarations
    
    //begin double declarations
    double wheelDiameter = 27.0, //diameter of bike wheel
           PI = 3.14159, //PI constant
           feetPerMile = 5280, //number of feet in one standard miles
           inchesPerFoot = 12, //number of inches in one standard foot
           secondsPerMinute = 60; //number of seconds in one standard minute
           double distanceTrip1, distanceTrip2, totalDistance; //variables used to store calculations
    //end double declarations
    
    //print time and rotations for trip 1 and 2
    //also calculates time in minutes via time in seconds.
    System.out.println("Trip 1 took: "+(secsTrip1/secondsPerMinute)+ " minutes and had "+countsTrip1+" counts."); //trip 1 time and rotation counts
    System.out.println("Trip 2 took: "+(secsTrip2/secondsPerMinute)+ " minutes and had "+countsTrip2+" counts."); //trip 2 time and rotation counts
    //end time and rotation
    
    //calculate total distance traveled using Pi (constant), Rotation Counts (input), and wheel diameter (input).calculate
    //finds distance traveled by each rotation, then multiplies by # of rotations.finds
    
    //calculate distance (inches) for trip 1
     distanceTrip1 = countsTrip1*wheelDiameter*PI;
    //end trip 1 distance
    
    //calculate distance (inches) for trip 2
    distanceTrip2 = countsTrip2*wheelDiameter*PI;
    //end trip 2 distance
    
    //calulate total distance in miles
    
    //trip 1 distance, in miles.
    distanceTrip1/=inchesPerFoot*feetPerMile;
    
    //trip 2 distance, in miles.
    distanceTrip2/=inchesPerFoot*feetPerMile;
    
    //end distance (miles) calculations.
    
    //compute total distance 
    totalDistance = distanceTrip1 + distanceTrip2;
    //end total distance computation
    
    //print individual distances
    
    //trip 1
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    //end trip 1
    
    //trip 2
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    //end trip 2
    
    //end individual distance print statements
    
    //print total distance
    
    System.out.println("The total distance was "+totalDistance+" miles");
    
    //end total distance printed.

    
    
    
    
    
    
    
  } //main method end
} //main class end