/**
Cody Benkoski
Lab 05
10-3-2018
**/

import java.util.Scanner;

public class Lab05
{
  public static void main(String args[])
  {
    int CRN; //course number
    String DeptName; //department course is in
    int Meetings; //how many times it meets in a week
    int beginTime; //time course begins, in 24 format
    String instructorName;
    int numStudents;
    String junk; //used to store bad buffer data
    
    Scanner scnr = new Scanner(System.in);
    System.out.println("What is the course number?: ");
    CRN = 0; //initialize before while
    Meetings = 0;
    beginTime = 0; 
    while(!scnr.hasNextInt())
    {
      System.out.println("Invalid course number, please try again");
      junk = scnr.nextLine();
    }
    CRN = scnr.nextInt(); //get the int
    junk = scnr.nextLine();
    
    System.out.println("What is the department the course is in?: ");
    DeptName = scnr.nextLine();
    junk = scnr.nextLine();
  
    System.out.println("How many times does the class meet?: ");
    do
    { 
      
      if(scnr.hasNextInt())
      {
        Meetings = scnr.nextInt();
        break;
      }
      else
      {
        System.out.println("Invalid time!, try again!");
        junk = scnr.nextLine();
      }
    }
    while(!scnr.hasNextInt());
junk = scnr.nextLine();
    System.out.println("When does the class begin?: ");
    do
    { 
      
      if(scnr.hasNextInt())
      {
        beginTime = scnr.nextInt();
        break;
      }
      else
      {
        System.out.println("Invalid time!, try again!");
        junk = scnr.nextLine();
      }
    }
    while(!scnr.hasNextInt());
    junk = scnr.nextLine();
    
    System.out.println("Enter the instructor's name: ");
    instructorName = scnr.nextLine();
    
    System.out.println("Enter the number of students in the class: ");
    while(!scnr.hasNextInt())
    {
      System.out.println("Invalid number of students! Try again.");
      junk = scnr.nextLine();
    }
    numStudents = scnr.nextInt();
    
    System.out.println("Course Number: "+CRN+" Department Name: "+DeptName+" How many times does it meet: "+Meetings+" When does the class begin (24 format): "+beginTime+" Instructor Name: "+instructorName+" Number of students: "+numStudents);
  }
}