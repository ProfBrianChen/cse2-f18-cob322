/**
Cody Benkoski
Hw 10
11-30-18
Tic-Tac-Toe
**/

import java.util.Scanner;
public class HW10
{
	public static void checkDraw(int index)
	{
				if(index == 9)
		{
			System.out.println("\nIts a draw!");
			System.exit(0);
		}
	}
	public static boolean checkForWinner(char [] [] board, String [] winArr)
	{
		//check for winners going down
		for(int column =0; column < 3; column++)
		{
			if(board[0][column] == board[1][column] && board[2][column] == board[1][column])
			{
				winArr[0] = Character.toString(board[0][column]);
				System.out.println(winArr[0]);
				winArr[1] = "vertical";
				return true;
			}
		}

		//check diagonals
		if(board[0][0] == board[1][1] && board[2][2] == board[1][1])
		{
			winArr[0] = Character.toString(board[0][0]);
			winArr[1] = "diagonal";
			return true;
		}
		if(board[0][2] == board[1][1] && board[2][0] == board[1][1])
		{
			winArr[0] = Character.toString(board[0][2]);
			winArr[1] = "diagonal";
			return true;
		}
		//check rows
		for(int rows =0; rows < 3; rows++)
		{
			if(board[rows][0] == board[rows][1] && board[rows][1] == board[rows][2])
			{
				winArr[0] = Character.toString(board[rows][1]);
				winArr[1] = "horizontal";
				return true;
			}
		}

		return false;
	}
	public static void promptPlayer(char whosUp) //no need to really check for bad input here since the user has no effect as to whats passed here
	{
		System.out.println();
		if(whosUp == 'O')
			System.out.println("Make your move Player 1: ");
		else
			System.out.println("Make your move Player 2: ");
	}
	public static char checkValidInput(char [] [] board)
	{
		Scanner input = new Scanner(System.in);
		System.out.println();
		String junk = "";
		while(!input.hasNextInt())
		{
			System.out.println("You must enter a valid space on the board, try again: ");
			junk = input.nextLine();
		}
		int choice = input.nextInt();
		while(!(choice > 0) || !(choice < 10))
		{
			//displayBoard(board);
			System.out.println("You must enter a valid space on the board, try again: ");
			junk = input.nextLine();
			while(!input.hasNextInt())
		{
			System.out.println("You must enter a valid space on the board, try again: ");
			junk = input.nextLine();
		}
		choice = input.nextInt();
		System.out.print(choice);
		}
		return (char)choice;

	}
	public static void displayBoard(char [] [] board)
	{
		for(int i = 0; i < board.length; i++)
		{
			System.out.println();
			for(int k = 0; k < board[i].length; k++)
			{
				System.out.print(board[i][k]+"   ");
			}
		}
	}

	public static boolean isValidMove(char [] [] board, char play)
	{
		int index = 0;
		int numPlay = (int)play; //cast the char number to an int so we can compare it easier
		if(numPlay < 4)
		{
			numPlay = numPlay -1;
			index = 0;
		}
		else if(numPlay < 7)
		{
			index = 1;
			numPlay = numPlay -4;
		} 
		else
		{
			index = 2;
			numPlay = numPlay - 7;
		}
		if((board[index][numPlay]) == 'X' || (board[index][numPlay]) == 'O')
		{ //if spot has already been played, return false
			System.out.println("\nThis spot has already been played. Try again.");
			return false;
		}
		else
			return true;
	}
	public static void playerOneMove(char choice, char [] [] board) //player 1 is O
	{
		int index = 0;
		int numPlay = (int)choice;
		if(numPlay < 4)
		{
			numPlay = numPlay -1;
			index = 0;
		}
		else if(numPlay < 7)
		{
			index = 1;
			numPlay = numPlay - 4;
		} 
		else
		{
			index = 2;
			numPlay = numPlay - 7;
		}
		board[index][numPlay] = 'O';

	}
	public static void playerTwoMove(char choice, char [] [] board) //player 2 is X
	{
		int index = 0;
		int numPlay = (int)choice;
		if(numPlay < 4)
		{
			numPlay = numPlay -1;
			index = 0;
		}
		else if(numPlay < 7)
		{
			index = 1;
			numPlay = numPlay - 4;
		} 
		else
		{
			index = 2;
			numPlay = numPlay - 7;
		}
		board[index][numPlay] = 'X';
	}
	public static void main(String args[])
	{
		int index = 0;
		String player = "";
		char whosUp = 'O';
		String [] winArr = {" "," "}; 
		boolean gameOver = false;
		boolean canPlace = true;
		char [][] board = {{'1','2','3'},{'4','5','6'},{'7','8','9'}};
		char choice = ' ';
		//displayBoard(board);
		while(!gameOver)
		{
			checkDraw(index);
			index++;
			displayBoard(board);
			promptPlayer(whosUp);	
			choice = checkValidInput(board);
			canPlace = isValidMove(board, choice);
			while(!canPlace)
			{
				promptPlayer(whosUp);	
				choice = checkValidInput(board);
				canPlace = isValidMove(board, choice);	
			}
			switch(whosUp)
			{
				case 'O': playerOneMove(choice, board);  whosUp='X'; break;
				case 'X': playerTwoMove(choice, board); whosUp = 'O';break;
			}
			gameOver = checkForWinner(board, winArr);

		}
		if(winArr[0].equals("O"))
			player = "Player 1";
		else
			player = "Player 2";

		System.out.println(player+" won by "+winArr[1]);

 	}
}