/**
Cody Benkoski
CSE 002 
11-12-2018
HW 08
**/
import java.util.Scanner;
public class Shuffling{ 

public static void printArray(String[] cards)
{
	System.out.println();
	for (int index = 0; index < cards.length;index++ ) 
	{
		System.out.print(cards[index]+" ");	
	}
	System.out.println();
}
public static void shuffle(String[] cards, int numCards) //overload method to shuffle nonstandard deck
{
	String tempSwitch = "";
	int numSwap = numCards+10;
	int index1 = 0;
	int index2 = 1;
	int length = cards.length -1;
	for (int i = 0; i < numSwap ;i++ ) 
	{
		index1 = (int)(Math.random() * ((length) + 1));	 //generate random numbers
		index2 = (int)(Math.random() * ((length) + 1));	
		//System.out.println(index1 +" "+index2);
		tempSwitch = cards[index1]; //give them a switch
		cards[index1] = cards[index2];
		cards[index2] = tempSwitch;
	}

}

public static void shuffle(String[] cards)
{
	String tempSwitch = "";
	int numSwap = 73;
	int index1 = 0;
	int index2 = 1;
	int length = cards.length -1;
	for (int i = 0; i < numSwap ;i++ ) 
	{
		index1 = (int)(Math.random() * ((length) + 1));	//generate random numbers
		index2 = (int)(Math.random() * ((length) + 1));	
		//System.out.println(index1 +" "+index2);
		tempSwitch = cards[index1]; //switch around the cards
		cards[index1] = cards[index2];
		cards[index2] = tempSwitch;
	}

}
public static int promptHand()
{
	int numCards = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the number of cards you want in your hand");
	while(!scan.hasNextInt())
	{
		System.out.println("You entered an invalid number of cards, try again");
	}
	numCards = scan.nextInt();
	return numCards;
}

public static String[] getHand(String[] cards, int index, int numCards)
{	
	System.out.println("Hand");
	String hand[] = new String[numCards];
	int indexTil = index - numCards;
	if(indexTil < 0) //generate a new hand if we've overshot our previous array
	{
		cards = createNewDeck(cards, numCards);
		indexTil = cards.length - numCards;
		//System.out.println(indexTil);
		int handIndex = 0; //have an auxiliary index so we can assign values in the other
		for (int i = cards.length; i > indexTil; indexTil++ )  //move to the back of the array
		{
			//System.out.print(indexTil);
			hand[handIndex] = cards[indexTil]+" "; //assign to hand
			handIndex++; //increment
		}
		return hand;
	}
	int handIndex = 0; //have an auxiliary index so we can assign values in the other
	for (int i = index; i > indexTil; indexTil++ ) 
	{
		hand[handIndex] = cards[indexTil]+" ";
		handIndex++;
	}
	return hand;

}

public static String[] createNewDeck(String[] cards, int numCards) //use given logic to generate a new deck
{
	 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
	for (int i=0; i<52; i++)
	{ 
  		cards[i]=rankNames[i%13]+suitNames[i/13]; 
	} 
	return cards;
}
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 0; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
//printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
	numCards = promptHand();
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 
}
