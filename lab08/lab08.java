/**
Cody Benkoski
CSE 002
Lab 08
**/

public class lab08
{
	public static void main(String args[])
	{
	 	int [] randomNumbers = new int[100];
	 	int [] occurences = new int[100];
	 	int randNum = 0;
	 	String timeSwitch = " time";

	 	for (int index = 0; index < 100; index++ ) 
	 	{
	 		randNum = (int) Math.floor(Math.random() * 100);
	 		randomNumbers[index] = randNum;	
	 	}

	 	for (int index = 0; index < 100; index++) 
	 	{	
	 		for (int num = 0; num < 100; num++ ) 
	 		{
	 			if(randomNumbers[index] == num)
	 			{
	 				occurences[num] +=1;
	 			}
	 		}
	 		if(occurences[index] == 0)
	 		{
	 			continue;
	 		}
	 		else if(occurences[index] > 2)
	 		{
	 			timeSwitch = " times";
	 		}
	 		else
	 		{
	 			timeSwitch = " time";
	 		}
	 		System.out.println(index +" occurs "+ occurences[index]+timeSwitch);
	 	}

	}
}