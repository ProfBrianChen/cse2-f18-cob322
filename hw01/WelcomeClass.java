/** 
Cody Benkoski
8-30-2018
CSE 002
**/
public class WelcomeClass 
{
   public static void main(String args[])
   {
        //start welcome
        System.out.println("  -----------");
        System.out.println("  | WELCOME |");
        System.out.println("  -----------");
        //end welcome
     
        //start signature
        System.out.println("  ^  ^  ^  ^  ^  ^");
        System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\"); //escape the escape characters.
        System.out.println("<-C--O--B--3--2--2->");
        System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); //escape the escape characters.
        System.out.println("  v  v  v  v  v  v");
        //end signature
     
        //start biography.
        System.out.println("Hi, my name is Cody Benkoski. I am from Wilkes-Barre, Pennsylvania, and pursuing a CSB major. My first few days haven't been too bad, I just feel like a rotisserie chicken in my dorm because of this  heat."); //biography, under 240 characters.
        //end biography
     
   }
}